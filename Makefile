
setup:
	pip install -U pip
	pip install -e .
	#git clone https://gitlab.com/nerdoc/pelican-clean-blog.git /$HOME/Projekte/pelican-clean-blog
	pelican-themes -s /$HOME/Projekte/pelican-clean-blog


serve:
	pelican -l -r

build:
	pelican

deploy: build
    # --dry-run
	rsync  -az --force --delete --progress --exclude-from=rsync_exclude.txt output/ ordination-gonzalez.at:/home/ordi/html/