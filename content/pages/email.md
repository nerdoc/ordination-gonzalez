title: Email
date: 2018-05-24 23:59
author: Christian González
slug: email
status: published
weight: 0
hide_in_menu: True

Liebe Patientinnen und Patienten,
liebe Kolleginnen und Kollegen!

Ich bekomme immer wieder Anfragen, ob ich Befunde per E-Mail verschicken kann,
oder Patienten fragen, ob sie mir Dokumente per E-Mail schicken dürfen.

Allein aus datenschutzrechtlichen Gründen ist das verboten. E-Mail sind nicht
verschlüsselt, können daher von vielen Personen (z.B. dem E-Mail-Provider) einfach mitgelesen oder
automationsunterstützt ausgewertet werden (Google, etc.)

Aus diesem Grund habe ich mich entschlossen, keine Ordinations-E-Mail-Adresse anzugeben.
Ich bitte um Ihr Verständnis. Patientendaten in digitaler Form können gerne per USB-Stick mit gebracht werden.

Für den Datenaustausch im medizinischen Bereich unterstützen wir DaMe (ME240845), und sind per Matrix für den
professionellen Austausch im Gesundheitswesen auf [medSpeak](https://medspeak.io) erreichbar.