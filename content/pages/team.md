---
title: Team
date: 2016-10-22 21:35
author: Christian González
slug: team
status: published
weight: 1
---


### Dr. Christian González
<span class="person-image pull-left">![Dr. Christian Gonzalez]({static}/images/christian.png)</span>

  * **Arzt für Allgemeinmedizin**
    * Ausbildung zum Allgemeinmediziner in Salzburg
    * Propädeutikum für Psychotherapie (Graz)
    * Medizinstudium (Graz)


### DGKS Gerlinde Dunhofer
<span class="person-image pull-left">![Gerlinde Dunhofer]({static}/images/gerlinde.png)</span>

* **Dipl. Gesundheits- und Krankenschwester, Kinderkrankenschwester**

### Christina Wahringer
<span class="person-image pull-left">![Christina Wahringer]({static}/images/christina.png)</span>

* **Ordinationsassistentin**
* **Dipl. Behindertenpädagogin**

### Barbara Pultar
<span class="person-image pull-left">![Barbara Pultar]({static}/images/barbara.png)</span>

* **Ordinationsassistentin**
* **Dipl. Ernährungsberaterin**