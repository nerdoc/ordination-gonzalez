#!/usr/bin/env python

AUTHOR = "Dr. Christian González"
SITENAME = "Ordination Dr. Christian González"
SITEURL = ""

SITE_LOGO = "images/logo.png"
HEADER_LOGO = "images/header_logo.png"
PATH = "content"

TIMEZONE = "Europe/Vienna"

DEFAULT_LANG = "de"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = True

# additional menu items
MENUITEMS = (("Startseite", "/"),)
PAGE_ORDER_BY = "weight"

SHOW_FULL_ARTICLE = True

# Blogroll
LINKS = (
    ("Datenschutz", "/pages/datenschutz.html"),
    ("Impressum", "/pages/impressum.html"),
)

# Social widget
SOCIAL = (("envelope", "/pages/email.html"), ("fax", "/pages/fax.html"))

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
STATIC_PATHS = ["images", "static"]

THEME = "pelican-clean-blog"
# see https://github.com/arulrajnet/attila#overview
HEADER_COVER = "images/wald-cropped.jpg"
COLOR_SCHEME_CSS = "monokai.css"
CSS_OVERRIDE = "static/css/custom.css"
CUSTOM_JS = "static/js/custom.js"
