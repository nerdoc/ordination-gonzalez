Quellcode zur Homepage [ordination-gonzalez.at](https://ordination-gonzalez.at).
Als Theme wird [pelican-clean-blog](https://gitlab.com/nerdoc/pelican-clean-blog)
verwendet.

### Installation

Auf einem [Uberspace](https://uberspace.de) Server ist folgendes zu tun:

Erstellen eines Deploy keys:

    ssh-keygen -f $HOME/.ssh/id_deploy
    cat $HOME/.ssh/id_deploy.pub

Dieser Key muss dann im [Gitlab repository](https://gitlab.com/nerdoc/ordination-gonzalez/-/settings/repository)
unter "Bereitstellungsschlüssel" hinzugefügt werden.

    cd ~
    git clone https://gitlab.com/nerdoc/ordination-gonzalez.git
    git clone https://gitlab.com/nerdoc/pelican-clean-blog.git
    cd ordination-gonzalez
    pip install

    pelican-themes -s $HOME/pelican-clean-blog

    # make the html directory our standard output dir!
    ln -s /var/www/virtual/$USER/html output

Lokal sollte man sich ebenfalls ein Repository klonen + das Theme. Dann kann
vom lokalen Repo das `deploy.sh` Skript ausgeführt werden, um eingepflegte
Änderungen in die live-Version zu übernehmen. Die SSH-Verbindung sollte da bereits funktionieren.

### Development

Installation der Requirements:

    pip install -U pip
    pip install -r requirements

    pelican-themes -s $HOME/Projekte/pelican-clean-blog  # wo auch immer das Theme liegt...

Für die Entwicklung startet man
     
    pelican -l -r
     
und öffnet http://127.0.0.1:8000 im Browser. Damit werden Änderungen sofort sichtbar. 