title: Impressum
date: 2016-09-27 16:01
author: Christian González
slug: impressum
status: published
weight: 5
hide_in_menu: True

**Dr. Christian González**  
Arzt für Allgemeinmedizin  
Salzachtal Bundesstraße 130/17  
5081 Niederalm

Alle Kassen

T: +43 6246 73416  
F: +43 6246 73416 - 20  
Website: https://ordination-gonzalez.at

Kammermitgliedschaft: [Ärztekammer Salzburg](http://www.aeksbg.at/)
