title: Datenschutz
date: 2018-05-24 23:59
author: Christian González
slug: datenschutz
status: published
weight: 10
hide_in_menu: True

### laut DSGVO 2018
Datenschutzverantwortlicher: Dr. Christian González

Mir liegen Ihre Daten sehr am Herzen. Ich nehme nicht nur den Datenschutz, sondern
auch die Datensparsamkeit sehr ernst. Diese Website protokolliert keinen
Zugriff, es werden keine IP-Adressen oder sonstige personenbezogenen Daten gespeichert, aus einem
einfachen Grund: ich brauche diese Daten nicht, ich analysiere kein Nutzerverhalten,
ich tracke Seitenbenutzer nicht. Diese Website verwendet keine Cookies.
Auf eine Kontaktemailadresse habe ich aus [Gründen](email.html) verzichtet.

Das Hosting der Website erfolgt auf [Uberspace](https://www.uberspace.de), dort
wird serverseitig die (maskierte, daher nicht personenbezogene) IP-Adresse,
Datum und Uhrzeit, sowie der URL-Pfad gespeichert.
Auf [deren Datenschutzseite](https://uberspace.de/privacy) kann man das auch genau nachlesen.
