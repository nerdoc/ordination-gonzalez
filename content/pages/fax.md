title: Fax
date: 2024-04-16 14:30
author: Christian González
slug: fax
status: published
weight: 0
hide_in_menu: True

Sehr geehrte Kommunikationspartner/-partnerinnen im Gesundheitssystem!

<strong>Spätestens ab 1.1.2025 ist das Versenden von Faxnachrichten mit medizinischen (sensiblen) Daten strikt verboten.</strong>

Daher setzen wir die veraltete Fax-Technologie nicht mehr ein - unsere bisherige Faxdurchwahl ist ab Januar 2025 nicht mehr erreichbar.
<hr>
Für den professionellen, interdisziplinären digitalen Informationsaustausch setzt unsere Ordination
auf [medSpeak](https://medspeak.io) - Falls Sie auch im Gesundheitssystem arbeiten, können Sie dort einen Account
beantragen.

<a href="https://medspeak.io" style="cursor:pointer">
![medSpeak]({static}/images/medspeak.png)</a>

<style>
a img:hover {
cursor:pointer;
background: lightgray;
border-radius: 10px;
}
</style>